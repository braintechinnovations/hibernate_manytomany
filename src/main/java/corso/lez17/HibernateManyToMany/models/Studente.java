package corso.lez17.HibernateManyToMany.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table
public class Studente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="StudenteID")
	private int id;
	
	@Column
	private String nominativo;
	@Column
	private String matricola;
	
	@ManyToMany
	@JoinTable(name="studente_esame",
			joinColumns = { @JoinColumn(name="StudenteIdRif", referencedColumnName = "StudenteID") },
			inverseJoinColumns = { @JoinColumn(name="EsameIdRif", referencedColumnName = "EsameID") })
	private List<Esame> esami;
	
	public Studente(String nominativo, String matricola) {
		super();
		this.nominativo = nominativo;
		this.matricola = matricola;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNominativo() {
		return nominativo;
	}
	public void setNominativo(String nominativo) {
		this.nominativo = nominativo;
	}
	public String getMatricola() {
		return matricola;
	}
	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	public List<Esame> getEsami() {
		return esami;
	}
	public void setEsami(List<Esame> esami) {
		this.esami = esami;
	}
	
	
}
