package corso.lez17.HibernateManyToMany.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table
public class Esame {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="EsameID")
	private int id;
	
	@Column
	private String nome_esame;
	@Column
	private int crediti;
	@Column
	private String data_esame;			//TODO: Guardare il tipo Date
	
	@ManyToMany
	@JoinTable(name = "studente_esame",
			joinColumns = { @JoinColumn(name="EsameIdRif", referencedColumnName = "EsameID") },
			inverseJoinColumns = { @JoinColumn(name="StudenteIdRif", referencedColumnName = "StudenteID") })
	private List<Studente> studenti;
	
	public Esame(String nome_esame, int crediti, String data_esame) {
		super();
		this.nome_esame = nome_esame;
		this.crediti = crediti;
		this.data_esame = data_esame;
	}
	public List<Studente> getStudenti() {
		return studenti;
	}
	public void setStudenti(List<Studente> studenti) {
		this.studenti = studenti;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome_esame() {
		return nome_esame;
	}
	public void setNome_esame(String nome_esame) {
		this.nome_esame = nome_esame;
	}
	public int getCrediti() {
		return crediti;
	}
	public void setCrediti(int crediti) {
		this.crediti = crediti;
	}
	public String getData_esame() {
		return data_esame;
	}
	public void setData_esame(String data_esame) {
		this.data_esame = data_esame;
	}
	
}
