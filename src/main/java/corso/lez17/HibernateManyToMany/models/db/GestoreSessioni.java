package corso.lez17.HibernateManyToMany.models.db;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import corso.lez17.HibernateManyToMany.models.Esame;
import corso.lez17.HibernateManyToMany.models.Studente;

public class GestoreSessioni {
	private SessionFactory factory;
	private static GestoreSessioni ogg_gestore;
	
	public static GestoreSessioni getGestore() {
		if(ogg_gestore == null) {
			ogg_gestore = new GestoreSessioni();
		}
		
		return ogg_gestore;
	}

	public SessionFactory getFactory() {
		if(factory == null) {
			factory = new Configuration()
					.configure("/resources/hibernate_universita.cfg.xml")
					.addAnnotatedClass(Studente.class)
					.addAnnotatedClass(Esame.class)
					.buildSessionFactory();
		}
		
		return factory;
	}
}
