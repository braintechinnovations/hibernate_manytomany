package corso.lez17.HibernateManyToMany;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import corso.lez17.HibernateManyToMany.models.Esame;
import corso.lez17.HibernateManyToMany.models.Studente;
import corso.lez17.HibernateManyToMany.models.db.GestoreSessioni;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	SessionFactory factory = GestoreSessioni.getGestore().getFactory();
		Session sessione = factory.getCurrentSession();
		
		try {
			sessione.beginTransaction();	
			
			Studente st_uno = new Studente("Duilio", "Minichetti");
			st_uno.setEsami(new ArrayList<Esame>());					//Costruzione rapida di un contenitore non inizializzato all'interno della classe!
			
			Esame es_uno = new Esame("Anatomia I", 12, "2021-06-04");
			es_uno.setStudenti(new ArrayList<Studente>());
			
			es_uno.getStudenti().add(st_uno);
			
			sessione.save(st_uno);
			sessione.save(es_uno);
			
			sessione.getTransaction().commit();
			
		} catch (Exception errore) {
			System.out.println(errore.getMessage());
		} finally {
			sessione.close();					
			System.out.println("Connessione Chiusa!");
		}
		
		//TODO: Ristruttura con DAO e Controller!
    }
}
